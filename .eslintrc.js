// In a file called .eslintrc.js
var fs = require('fs')

module.exports = {
  parser: "babel-eslint",
  rules: {
    "graphql/template-strings": ['error', {
      env: 'apollo',
      schemaString: fs.readFileSync('./schema.graphql', { encoding: 'utf8' }),
      tagName: 'gql'
    }],
    "comma-dangle": ["error", "always-multiline"],
    "react/prop-types": ["off"]
  },
  globals: {
    "fetch": true,
    "it": true,
  },
  plugins: [
    'graphql',
    'react',
  ],
  extends: ["standard", "standard-react"]
}