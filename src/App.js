import React, { Component } from 'react'
import MessageForm from './MessageForm'
import Channels from './Channels'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      channelId: '',
    }

    this.onChannelSelection = this.onChannelSelection.bind(this)
  }

  onChannelSelection (channelId) {
    this.setState({
      channelId,
    })
  }

  render () {
    return (
      <div>
        <Channels onChannelSelection={this.onChannelSelection} />
        {this.state.channelId !== '' ? <div style={{ float: 'right', width: '60%', height: '700px', borderLeftWidth: 5 }} >
          <MessageForm key={this.state.channelId} channelId={this.state.channelId} />
        </div> : null}
      </div>
    )
  }
}

export default App
