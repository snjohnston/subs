import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

const query = gql`
query getChannels {
  channelIds
}
`

const sub = gql`
subscription channelAdded {
  channelAdded
}
`

class SingleChannel extends Component {
  render () {
    return (
      <button
        onClick={() => this.props.onChannelSelection(this.props.channelId)}
        style={{ width: '60%', borderWidth: '1px', borderStyle: 'solid', borderRadius: '5px', }} >
        Channel ID: {this.props.channelId}
      </button>
    )
  }
}

class Channels extends Component {
  componentWillMount () {
    this.props.data.subscribeToMore({
      document: sub,
      variables: {},
      updateQuery: (prev, { subscriptionData }) => {
        if(!subscriptionData.data){
          return prev
        }

        const newChannel = subscriptionData.data.channelAdded

        return {
          ...prev,
          channelIds: [
            ...prev.channelIds,
            newChannel,
          ],
        }
      }
    })
  }


  render () {
    if (this.props.data.channelIds) {
      return (
        <div style={{ position: 'absolute', height: '100%', width: '40%' }} >
          <h1>CHANNELS</h1>
          {this.props.data.channelIds.map((channelId) => <SingleChannel
            key={`${channelId}`}
            channelId={channelId}
            onChannelSelection={this.props.onChannelSelection} />)}
        </div>
      )
    } else {
      return null
    }
  }
}

export default graphql(query)(Channels)
