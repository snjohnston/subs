import React, { Component } from 'react';
import Messages from './Messages'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

const addMessage = gql`
mutation addMessage($message: MessageInput!) { 
  addMessage(message: $message){
    text
    id
  }
}
`

class MessageForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      message: '',
    }
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  handleSubmit (e) {
    e.preventDefault()
    this.props.mutate({
      variables: { message: { channelId: this.props.channelId, text: this.state.message } }
    })
    this.setState({
      message: '',
    })
  }

  handleChange (event) {
    this.setState({
      message: event.target.value,
    })
  }

  render () {
    return (
      <div style={{ height: '90%' }} >
        <h1>{this.props.channelId}</h1>
        <div style={{ height: '100%', overflowY: 'scroll' }} >
          <Messages channelId={this.props.channelId} />
        </div>
        <form onSubmit={this.handleSubmit}>
          <input type="text" name="name" style={{ width: '100%', height: 40 }} value={this.state.message} onChange={this.handleChange} />
        </form>
      </div>
    )
  }
}

export default graphql(addMessage)(MessageForm)
