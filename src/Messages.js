import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import SingleMessage from './SingleMessage'

const sub = gql`
subscription getNewMessages($channelId: ID!) { 
  messageAdded(channelId: $channelId){
    text
    id
  }
}
`

const query = gql`
query getMessages($id: ID!) { 
  channel(id: $id){
    messages {
      text
      id
    }
  }
}
`

class Messages extends Component {
  componentWillMount () {
    this.props.data.subscribeToMore({
      document: sub,
      variables: {
        channelId: this.props.channelId
      },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) {
          return prev
        }

        const newMessage = subscriptionData.data.messageAdded
        return {
          ...prev,
          channel: {
            ...prev.channel,
            messages: [...prev.channel.messages, newMessage],
          }
        }
      }
    })
  }

  render () {
    if (this.props.data.channel) {
      return (
        <div>
          {this.props.data.channel.messages.map((message) => <SingleMessage key={`${message.id}.${message.text}`} message={message.text} />)}
        </div>
      )
    } else {
      return null
    }
  }
}

export default graphql(query, {
  options: (props) => {
    return { variables: { id: props.channelId } }
  }
})(Messages)
