import React, { Component } from 'react'


export default class SingleMessage extends Component {
  render () {
    return <p style={{ width: '60%', borderWidth: '1px', borderStyle: 'solid', borderRadius: '5px', }} >{this.props.message}</p>
  }
}
